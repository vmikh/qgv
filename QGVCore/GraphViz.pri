#GraphViz librairie
DEFINES += WITH_CGRAPH
INCLUDEPATH += private
QMAKE_CXXFLAGS += -DQGVCORE_LIB

QT_CONFIG -= no-pkg-config

unix {
 CONFIG += link_pkgconfig
 CONFIG += c++11
 PKGCONFIG += libcdt libgvc libcgraph
}

win32 {
 # Try to use qmake variable's value.
 GRAPHVIZ_PATH = $$GRAPH_VIZ_PATH
 isEmpty(GRAPHVIZ_PATH) {
    message(\"Graphviz Library\" qmake value not detected...)

    # Try to use the system environment value.
    GRAPHVIZ_PATH = $$(GRAPH_VIZ_PATH)
 }
 isEmpty(GRAPHVIZ_PATH) {
    message(\"Graphviz Library\" env value not detected...)
 }

 #Configure Windows GraphViz path here :
 DEFINES += WIN32_DLL
 DEFINES += GVDLL
 INCLUDEPATH += $$GRAPHVIZ_PATH/include/graphviz
 LIBS += -L$$GRAPHVIZ_PATH/lib/release/lib -lgvc -lcgraph -lgraph -lcdt
}
