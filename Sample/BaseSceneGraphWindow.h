#ifndef ASCENEGRAPHWINDOW
#define ASCENEGRAPHWINDOW

#include<QWidget>

class QGraphicsViewEc;
class QGVScene;

class BaseSceneGraphWindow;

namespace SceneGraph
{
    BaseSceneGraphWindow* create(QWidget *parent = 0);
    BaseSceneGraphWindow* create(QWidget *parent, Qt::WindowFlags f);
}

class BaseSceneGraphWindow : public QWidget
{
    Q_OBJECT
protected:
    friend BaseSceneGraphWindow* SceneGraph::create(QWidget *parent, Qt::WindowFlags f);
    BaseSceneGraphWindow(QWidget *parent = 0, Qt::WindowFlags f = 0);
public:
    virtual ~BaseSceneGraphWindow();

    void openFile(const QString& fileName, bool showFileName = false);
    void exportTo(const QString& fileName);

    void zoom(int delta);
    void vertivalScrollPosChanged(QObject* sender, int pos);
    void horizontalScrollPosChanged(QObject* sender, int pos);

    void costomizeZoom(QObject* filter);

    QObject* vertivalScroll() const;
    QObject* horizontalScroll() const;


signals:
    void toBeDestroyed(const QString&);

protected:
    virtual bool eventFilter(QObject* pobj, QEvent* pe) override;
    virtual void closeEvent ( QCloseEvent * event ) override;
private:
    QWidget* sceneWidget_;
    QGraphicsViewEc* graphicsView_;
    QGVScene* scene_;
};
#endif // ASCENEGRAPHWINDOW

