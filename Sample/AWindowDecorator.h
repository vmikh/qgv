#ifndef WINDOWSTORAGE_H
#define WINDOWSTORAGE_H

class MainWindow;
class BaseSceneGraphWindow;

class AMainWindowDecorator;
class CompareWindow;

namespace WD {
    AMainWindowDecorator* create(MainWindow* ui);
}

class AMainWindowDecorator
{
public:
    explicit AMainWindowDecorator(MainWindow* mainWindow);
    virtual ~AMainWindowDecorator();

    void customizeMainUi();
    void addSceneWindow(BaseSceneGraphWindow* scWin);
    void removeSceneWindow(const QString& title);
    void activateSceneWindow(const QString& title);
    CompareWindow* createCW();

private:
    virtual void doCustomizeMainUi() = 0;
    virtual void doAddSceneWindow(BaseSceneGraphWindow* scWin) = 0;
    virtual void doRemoveSceneWindow(const QString& title) = 0;
    virtual void doActivateSceneWindow(const QString& title) = 0;
    virtual CompareWindow* doCreateCW() = 0;
protected:
    MainWindow* mainWindow_;
 };
#endif // WINDOWSTORAGE_H
