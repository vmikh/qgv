#include "BaseSceneGraphWindow.h"

#include <QFile>
#include <QFileInfo>
#include <QVBoxLayout>
#include <QMainWindow>
#include <QScrollBar>
#include <QWheelEvent>

#include "QGraphicsViewEc.h"
#include "QGVScene.h"

BaseSceneGraphWindow::BaseSceneGraphWindow(QWidget *parent, Qt::WindowFlags f) : QWidget(parent, f)
{
    sceneWidget_ = new QWidget(parent);

    QVBoxLayout* verticalLayout = new QVBoxLayout(sceneWidget_);
    verticalLayout->setSpacing(6);
    verticalLayout->setContentsMargins(11, 11, 11, 11);

    scene_ = new QGVScene("DEMO", this);

    graphicsView_ = new QGraphicsViewEc(sceneWidget_);
    graphicsView_->setRenderHints(QPainter::Antialiasing|QPainter::SmoothPixmapTransform|QPainter::TextAntialiasing);
    graphicsView_->setDragMode(QGraphicsView::ScrollHandDrag);
    graphicsView_->setScene(scene_);

    verticalLayout->addWidget(graphicsView_);
    this->setLayout(verticalLayout);

    setAttribute(Qt::WA_DeleteOnClose);

    graphicsView_->installEventFilter(this);
}

BaseSceneGraphWindow::~BaseSceneGraphWindow()
{

}

void BaseSceneGraphWindow::openFile(const QString &fileName, bool showFileName)
{
    if (fileName.isNull())
        return;

    QFile dotFile(fileName);
    if (!dotFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QByteArray data = dotFile.readAll();
    scene_->clear();
    scene_->loadLayout(data.constData(), showFileName ? fileName: QString());

    setWindowFilePath(fileName);
    setWindowTitle(fileName);
}

void BaseSceneGraphWindow::exportTo(const QString& fileName)
{
    if (fileName.isNull())
        return;

    scene_->exportTo(fileName, QFileInfo(fileName).suffix());
}

void BaseSceneGraphWindow::zoom(int delta)
{
    graphicsView_->zoom(delta);
}

void BaseSceneGraphWindow::vertivalScrollPosChanged(QObject* sender, int pos)
{
    //skip self update
    if (graphicsView_->verticalScrollBar() != sender)
        graphicsView_->verticalScrollBar()->setValue(pos);
}

void BaseSceneGraphWindow::horizontalScrollPosChanged(QObject* sender, int pos)
{
    //skip self update
    if (graphicsView_->horizontalScrollBar() != sender)
        graphicsView_->horizontalScrollBar()->setValue(pos);
}

QObject* BaseSceneGraphWindow::vertivalScroll() const
{
    return graphicsView_->verticalScrollBar();
}

QObject* BaseSceneGraphWindow::horizontalScroll() const
{
    return graphicsView_->horizontalScrollBar();
}

bool BaseSceneGraphWindow::eventFilter(QObject*, QEvent* pe)
{
    if (pe->type() == QEvent::Wheel)
    {
        graphicsView_->zoom(((QWheelEvent*)pe)->delta());

        return true;
    }
    return false;
}

void BaseSceneGraphWindow::closeEvent(QCloseEvent * )
{
    emit toBeDestroyed(this->windowTitle());
}

void BaseSceneGraphWindow::costomizeZoom(QObject* filter)
{
    graphicsView_->removeEventFilter(this);
    graphicsView_->installEventFilter(filter);
}
