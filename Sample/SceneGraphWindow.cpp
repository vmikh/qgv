#include "SceneGraphWindow.h"

#include <QFile>
#include <QFileInfo>
#include <QVBoxLayout>
#include <QMainWindow>
#include <QScrollBar>
#include <QWheelEvent>

#include "QGraphicsViewEc.h"
#include "QGVScene.h"
#include "BaseSceneGraphWindow.h"

namespace SceneGraph
{
    BaseSceneGraphWindow* create(QWidget* parent, Qt::WindowFlags f)
    {
        return new BaseSceneGraphWindow(parent, f);
    }

    #ifdef __APPLE__
    BaseSceneGraphWindow* create(QWidget* parent)
    {
        return create(parent, Qt::Window);
    }
    #endif

    #ifdef _WIN32
    BaseSceneGraphWindow* create(QWidget* parent)
    {
        return create(parent, Qt::SubWindow);
    }
    #endif


}


SceneGraphWindowMac::SceneGraphWindowMac(QWidget *parent, Qt::WindowFlags f) : BaseSceneGraphWindow(parent, f)
{

}


SceneGraphWindowWin::SceneGraphWindowWin(QWidget *parent, Qt::WindowFlags f) : BaseSceneGraphWindow(parent, f)
{

}
