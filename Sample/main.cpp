/***************************************************************
QGVCore Sample
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/
#include<QFile>
#include<QDir>
#include <QMessageBox>
#include <QFileOpenEvent>

#include "MainWindow.h"
#include "qtsingleapplication.h"

const QString appName = "QGV";

class QgvApplication : public QtSingleApplication
{
public:
    QgvApplication(int &argc, char **argv) : QtSingleApplication(appName, argc, argv)
    {

    }

    virtual ~QgvApplication()
    {

    }

    QString getOpenedFileName() const
    {
        return fileName_;
    }

    void setOpenFileHandler(MainWindow* w)
    {
        w_ = w;
    }

    void markOpenedFileDone()
    {
        fileName_.clear();
    }

protected:
    virtual bool event(QEvent* pe) override
    {
        if (QEvent::FileOpen == pe->type())
        {
            auto fo_event = static_cast<QFileOpenEvent *>(pe);

            if (fo_event)
            {
                if (w_)
                    w_->openFile(fo_event->file());
                else
                    fileName_ = fo_event->file();
            }

        }
        return QtSingleApplication::event(pe);
    }
private:
    QString fileName_;
    MainWindow* w_;
};

void register_application(QWidget* parent)
{
    const QString info_file_name = "info.txt";
    const QString info_file_subdir_name = "QGV";

    QDir info_file_subdir(QDir::homePath() + QDir::separator() + info_file_subdir_name);

    if(!info_file_subdir.exists())
        info_file_subdir.mkpath(info_file_subdir.path());

    QString info_file_path = info_file_subdir.path() + QDir::separator() + info_file_name;
    QFile infoFile(info_file_path);

    if (infoFile.open(QFile::WriteOnly | QFile::Truncate | QFile::Text))
    {
        QByteArray data;
        data.append(QApplication::applicationFilePath().toUtf8());
        infoFile.write(data);
    }
    else
    {
        QMessageBox::information(parent, QObject::tr("Warning"), QObject::tr("Can not create info file."));
    }
}

int main(int argc, char *argv[])
{
    QgvApplication app(argc, argv);
    app.setApplicationName(appName);

#ifdef _WIN32
    app.setWindowIcon(QIcon(":/icons/Graphviz.ico"));
#endif

    QString fileName;
    QStringList args = app.arguments();

    if (args.size() > 1)
    {
        // if fileName_ contains spaces but was not quoted it will be split into several "arguments"
        args.removeFirst();
        fileName = args.join(" ");
    }

    if (fileName.isEmpty())
    {
        fileName = app.getOpenedFileName();
        app.markOpenedFileDone();
    }

    if (app.sendMessage(fileName))
        return 0;

    MainWindow w;

    QObject::connect(&app, SIGNAL(messageReceived(const QString&)), &w, SLOT(onHandleMessage(const QString&)));
    app.setOpenFileHandler(&w);

    app.setActivationWindow(&w, false);

    QObject::connect(&w, SIGNAL(needToShow()), &app, SLOT(activateWindow()));

    w.show();

    register_application(&w);

    if (!fileName.isEmpty())
    {
        w.openFile(fileName);
    }

    return app.exec();
}
