#include <QDesktopWidget>

#include <QMdiArea>
#include <QMdiSubWindow>
#include <QSplitter>

#include "AWindowDecorator.h"
#include "BaseSceneGraphWindow.h"

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "CompareWindow.h"

AMainWindowDecorator::AMainWindowDecorator(MainWindow* mainWindow) : mainWindow_(mainWindow)
{

}

AMainWindowDecorator::~AMainWindowDecorator()
{

}

void AMainWindowDecorator::customizeMainUi()
{
    doCustomizeMainUi();
}

void AMainWindowDecorator::addSceneWindow(BaseSceneGraphWindow *scWin)
{
    doAddSceneWindow(scWin);
}

void AMainWindowDecorator::removeSceneWindow(const QString& title)
{
    doRemoveSceneWindow(title);
}

void AMainWindowDecorator::activateSceneWindow(const QString &title)
{
    doActivateSceneWindow(title);
}

CompareWindow* AMainWindowDecorator::createCW()
{
    return doCreateCW();
}

class MacMainWindowDecorator : public AMainWindowDecorator
{
public:
    explicit MacMainWindowDecorator(MainWindow* ui) : AMainWindowDecorator(ui)
    {

    }
    virtual ~MacMainWindowDecorator()
    {

    }

private:
    virtual void doCustomizeMainUi()
    {
        QDesktopWidget desktop;
        QRect desktopRect(desktop.availableGeometry(mainWindow_));

        QRect mainWindowRect(0, 0, desktopRect.width() / 6, desktopRect.height());
        mainWindow_->move(QPoint(0,0));
        mainWindow_->setGeometry(mainWindowRect);

        mainWindow_->setMaximumWidth(desktopRect.width() / 5);
    }

    virtual void doAddSceneWindow(BaseSceneGraphWindow *sgMac)
    {
        QDesktopWidget desktop;
        QRect desktopRect(desktop.availableGeometry(mainWindow_));
        QRect mainWindowRect(mainWindow_->rect());
        QRect sceneWindowRect(mainWindowRect.width(), 0, desktopRect.width() - mainWindowRect.width(), mainWindowRect.height());
        sgMac->setGeometry(sceneWindowRect);
        sgMac->show();
    }

    virtual void doRemoveSceneWindow(const QString& )
    {

    }

    virtual void doActivateSceneWindow(const QString& )
    {

    }

    virtual CompareWindow* doCreateCW()
    {
        return new CompareWindow(mainWindow_);
    }
};

class WinMainWindowDecorator : public AMainWindowDecorator
{
public:
    explicit WinMainWindowDecorator(MainWindow* ui) : AMainWindowDecorator(ui)
    {
        mdiArea_ = new QMdiArea(ui);
        mdiArea_->cascadeSubWindows();
        splitter_ = new QSplitter(ui);
    }
    virtual ~WinMainWindowDecorator()
    {

    }

private:
    virtual void doCustomizeMainUi()
    {
        mainWindow_->Ui()->horizontalLayout->addWidget(splitter_);

        splitter_->addWidget(mainWindow_->Ui()->graphListWidget);
        splitter_->addWidget(mdiArea_);

        splitter_->setStretchFactor(0, QSizePolicy::Fixed);
        splitter_->setStretchFactor(1, QSizePolicy::Expanding);

        QDesktopWidget desktop;
        QRect desktopRect(desktop.availableGeometry(mainWindow_));

        mainWindow_->move(QPoint(0,0));
        mainWindow_->setGeometry(desktopRect);
        mainWindow_->showMaximized();
    }

    virtual void doAddSceneWindow(BaseSceneGraphWindow *sgWin)
    {
        mdiArea_->addSubWindow(sgWin, Qt::SubWindow);
        sgWin->setGeometry(mdiArea_->rect());
        sgWin->showMaximized();
    }

    virtual void doRemoveSceneWindow(const QString& title)
    {
        for (auto i : mdiArea_->subWindowList())
        {
            if (i->windowTitle() == title)
            {
                mdiArea_->removeSubWindow(i);
                break;
            }
        }
    }

    virtual void doActivateSceneWindow(const QString& title)
    {
        for(auto window : mdiArea_->subWindowList())
        {
            if (window->windowTitle() == title)
            {
                window->showMaximized();
                window->activateWindow();
                window->raise();

            }
        }
    }

    virtual CompareWindow* doCreateCW()
    {
        return new CompareWindow(0);
    }

private:
    QMdiArea * mdiArea_;
    QSplitter * splitter_;
};


namespace WD {

    #ifdef __APPLE__
    AMainWindowDecorator* create(MainWindow* ui)
    {
        return new MacMainWindowDecorator(ui);
    }
    #endif
    #ifdef _WIN32
    AMainWindowDecorator* create(MainWindow* ui)
    {
        return new WinMainWindowDecorator(ui);
    }
    #endif
}
