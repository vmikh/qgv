#ifndef COMPAREWINDOW
#define COMPAREWINDOW

#include<QWidget>
#include<QString>
#include<QVector>
#include<QHash>
#include<QPointer>

class QMainWindow;
class BaseSceneGraphWindow;

class CompareWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CompareWindow(QMainWindow* parent);
    virtual ~CompareWindow();

    void process(const QVector<QString>& files);

protected:
    virtual bool eventFilter(QObject* pobj, QEvent* pe) override;
    virtual void resizeEvent(QResizeEvent* ev) override;

private slots:
    void verticalScrollPosChange(int pos);
    void horizontalScrollPosChange(int pos);
private:
    QHash<QString, QPointer<BaseSceneGraphWindow>> childWind_;
};

#endif // COMPAREWINDOW

