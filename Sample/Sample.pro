#-------------------------------------------------
#
# Project created by QtCreator 2013-04-17T09:06:06
#
#-------------------------------------------------

QT       += core gui
QT       *= network

greaterThan(QT_MAJOR_VERSION, 3): QT += widgets

TARGET = QGraphViz
VERSION = 0.2.0
TEMPLATE = app

DESTDIR = ../bin
DLLDESTDIR = ../bin

#QGVCore librairie
LIBS += -L$$OUT_PWD/../lib -lQGVCore
INCLUDEPATH += $$PWD/../QGVCore
DEPENDPATH += $$PWD/../QGVCore
#GraphViz librairie
!include(../QGVCore/GraphViz.pri) {
     error("fail open GraphViz.pri")
 }

#QtSingleApplication
LIBS += -L$$OUT_PWD/../lib -lQtSingleApplication
INCLUDEPATH += $$PWD/../lgeyers-qt-solutions/qtsingleapplication/src


SOURCES += main.cpp\
        MainWindow.cpp \
    QGraphicsViewEc.cpp \
    SceneGraphWindow.cpp \
    CompareWindow.cpp \
    AWindowDecorator.cpp \
    BaseSceneGraphWindow.cpp

HEADERS  += MainWindow.h \
    QGraphicsViewEc.h \
    SceneGraphWindow.h \
    CompareWindow.h \
    AWindowDecorator.h \
    BaseSceneGraphWindow.h

FORMS    += MainWindow.ui

RESOURCES += \
    ress.qrc


DEFINES += PRODUCT_VERSION=\\\"$$VERSION\\\"

QT_QMAKE_PATH = $$system_path($$QMAKE_QMAKE)
QT_PATH = $$replace(QT_QMAKE_PATH,"\\qmake.exe","")

unix{
    CONFIG(release, debug|release){
        PRO_PATH = $$OUT_PWD
        BUNDLE_PATH = $$replace(PRO_PATH,"Sample","bin")
        QMAKE_POST_LINK = macdeployqt $$BUNDLE_PATH/$$TARGET".app/"
    }

    macx{
        ICON=GraphViz.icns
        QMAKE_INFO_PLIST = Info.plist
    }
}

win32 {

qtlibs.path = $$OUT_PWD/../bin

qtlibs.files += \"$$QT_PATH\\icuuc53.dll\"
qtlibs.files += \"$$QT_PATH\\icuin53.dll\"
qtlibs.files += \"$$QT_PATH\\icudt53.dll\"

CONFIG(release, debug|release){
    qtlibs.files += \"$$QT_PATH\\Qt5Network.dll\"
    qtlibs.files += \"$$QT_PATH\\Qt5Core.dll\"
    qtlibs.files += \"$$QT_PATH\\Qt5Widgets.dll\"
    qtlibs.files += \"$$QT_PATH\\Qt5Gui.dll\"
}

CONFIG(debug, debug|release){
    qtlibs.files += \"$$QT_PATH\\Qt5Networkd.dll\"
    qtlibs.files += \"$$QT_PATH\\Qt5Cored.dll\"
    qtlibs.files += \"$$QT_PATH\\Qt5Widgetsd.dll\"
    qtlibs.files += \"$$QT_PATH\\Qt5Guid.dll\"
}

INSTALLS += qtlibs

QT_PLATFORM_PATH = $$replace(QT_PATH,"bin", "plugins\\platforms")
platforms.path = $$OUT_PWD/../bin/platforms
platforms.files += \"$$QT_PLATFORM_PATH\\\\qwindows.dll\"
INSTALLS += platforms

graphvizbins.path = $$OUT_PWD/../bin
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\Pathplan.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\QtCore4.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\QtGui4.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\ann.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\cdt.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\cgraph.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\fontconfig.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\fontconfig_fix.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\freetype6.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\ glut32.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\gvc.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\gvplugin_core.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\gvplugin_dot_layout.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\gvplugin_gd.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\gvplugin_gdiplus.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\gvplugin_neato_layout.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\gvplugin_pango.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\iconv.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\intl.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\jpeg62.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libatk-1.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libcairo-2.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libexpat-1.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libexpat.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libfontconfig-1.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libfreetype-6.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgdk-win32-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgdk_pixbuf-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgdkglext-win32-1.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgio-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libglade-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libglib-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgmodule-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgobject-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgthread-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgtk-win32-2.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libgtkglext-win32-1.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libltdl-3.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libpango-1.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libpangocairo-1.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libpangoft2-1.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libpangowin32-1.0-0.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libpng12.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libpng14-14.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\librsvg-2-2.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\libxml2.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\ltdl.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\vmalloc.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\zlib1.dll\"
graphvizbins.files += \"$$GRAPHVIZ_PATH\\bin\\config6\"
INSTALLS += graphvizbins
}
