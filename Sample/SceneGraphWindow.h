#ifndef MAINAPPLICATIONWINDOW
#define MAINAPPLICATIONWINDOW

#include <QMdiSubWindow>
#include "BaseSceneGraphWindow.h"

class QGraphicsViewEc;
class QGVScene;

class SceneGraphWindowMac : public BaseSceneGraphWindow
{
    Q_OBJECT
public:
    SceneGraphWindowMac(QWidget *parent = 0, Qt::WindowFlags f = 0);
};

class SceneGraphWindowWin : public BaseSceneGraphWindow
{
    Q_OBJECT
public:
    SceneGraphWindowWin(QWidget *parent = 0, Qt::WindowFlags f = 0);
};


#endif // MAINAPPLICATIONWINDOW

