/***************************************************************
QGVCore Sample
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHash>
#include <QPointer>
#include <QScopedPointer>

namespace Ui
{
    class MainWindow;
}

class BaseSceneGraphWindow;
class QListWidgetItem;
class CompareWindow;
class AMainWindowDecorator;
class QActionGroup;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget* parent = 0);
    virtual ~MainWindow();

    void openFile(const QString& fileName);
    void exportTo(const QString& fileName);

    Ui::MainWindow* Ui();

private:
    void processGraph(const QString& fileName);
    void activateSelectedWindow(const QString& label);

public slots:
    void onHandleMessage(const QString& fileName);
    void onFileOpen();
    void onHelpAbout();
    void onExport();
    void onWindowMenuItemClick(QAction*);

private slots:
    void onClearSelected();
    void onItemDblCliked(QListWidgetItem* item);
    void onCompareItems();

    void onRaiseCompareWindow();
    void onDestroyCompareWindow();
    void onDestroySceneWindow(const QString&);

signals:
    void needToShow();

private:
    //Ui componets
    Ui::MainWindow* ui_;
    QActionGroup* windowListGroup_;
    QPointer<CompareWindow> diffWindow_;

    QScopedPointer<AMainWindowDecorator> mwDecorator_;
    QHash<QString, QPointer<BaseSceneGraphWindow>> sceneRegister_;
};

#endif // MAINWINDOW_H
