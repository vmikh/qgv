#include "CompareWindow.h"

#include <QMainWindow>
#include <QWheelEvent>
#include <QDesktopWidget>

#include "BaseSceneGraphWindow.h"

CompareWindow::CompareWindow(QMainWindow *parent) :
    QWidget(parent, Qt::Window)
{
    setWindowTitle(tr("Diff Window"));

    setAttribute(Qt::WA_DeleteOnClose);

    QDesktopWidget desktop;
    this->setGeometry(desktop.availableGeometry(this));
    installEventFilter(this);
}

CompareWindow::~CompareWindow()
{

}

void CompareWindow::process(const QVector<QString> &files)
{
    if (files.empty())
        return;

    QPoint position(0, 0);
    QRect rect(0, 0, this->width(), this->height() / files.count());

    for (auto item : files )
    {
        BaseSceneGraphWindow* sgWin = SceneGraph::create(this, Qt::Widget);

        //costumize zoom behaviour
        sgWin->costomizeZoom(this);

        childWind_.insert(item, sgWin);

        connect(sgWin->horizontalScroll(),  SIGNAL(valueChanged(int)), SLOT(horizontalScrollPosChange(int)));
        connect(sgWin->vertivalScroll(),  SIGNAL(valueChanged(int)), SLOT(verticalScrollPosChange(int)));

        sgWin->setGeometry(rect);
        sgWin->move(position);

        position.setY(position.y() + sgWin->height());

        sgWin->openFile(item, true);

        sgWin->showNormal();
    }
}

bool CompareWindow::eventFilter(QObject*, QEvent* pe)
{
    if (pe->type() == QEvent::Wheel)
    {
        for (auto item : childWind_.values())
        {
            if (item)
                item->zoom(((QWheelEvent*)pe)->delta());
        }
        return true;
    }
    return false;
}

void CompareWindow::resizeEvent(QResizeEvent *ev)
{
    if (childWind_.empty())
        return;

    QRect rect(0,0, ev->size().width(), ev->size().height() / childWind_.values().count());
    QPoint position(0, 0);

    for (auto sgWin : childWind_.values() )
    {

        sgWin->setGeometry(rect);
        sgWin->move(position);

        position.setY(position.y() + sgWin->height());

        sgWin->showNormal();
    }
}

void CompareWindow::verticalScrollPosChange(int pos)
{
    for (auto item : childWind_.values())
    {
        if (item)
            item->vertivalScrollPosChanged(sender(), pos);
    }
}

void CompareWindow::horizontalScrollPosChange(int pos)
{
    for (auto item : childWind_.values())
    {
        if (item)
            item->horizontalScrollPosChanged(sender(), pos);
    }
}


