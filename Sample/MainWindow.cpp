/***************************************************************
QGVCore Sample
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/
#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "QGVScene.h"
#include "QGVNode.h"
#include "QGVEdge.h"
#include "QGVSubGraph.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QListWidgetItem>
#include <QDesktopWidget>

#include "BaseSceneGraphWindow.h"
#include "CompareWindow.h"
#include "AWindowDecorator.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::MainWindow),
    mwDecorator_(WD::create(this)),
    diffWindow_(0)
{
    ui_->setupUi(this);
    ui_->graphList->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui_->actionDiffWindow->setVisible(false);

    connect(ui_->actExport, SIGNAL(triggered()), SLOT(onExport()));
    connect(ui_->actOpen, SIGNAL(triggered()), SLOT(onFileOpen()));
    connect(ui_->actAbout, SIGNAL(triggered()), SLOT(onHelpAbout()));

    connect(ui_->clearButton, SIGNAL(clicked()), SLOT(onClearSelected()));
    connect(ui_->graphList, SIGNAL(itemDoubleClicked(QListWidgetItem*)), SLOT(onItemDblCliked(QListWidgetItem*)));
    connect(ui_->compareButton, SIGNAL(clicked()), SLOT (onCompareItems()));
    connect(ui_->actionDiffWindow, SIGNAL(triggered()), SLOT(onRaiseCompareWindow()));

    diffWindow_ = mwDecorator_->createCW();
    connect(diffWindow_, SIGNAL(destroyed()), SLOT(onDestroyCompareWindow()));

    windowListGroup_ = new QActionGroup(this);
    QObject::connect(windowListGroup_, SIGNAL(triggered(QAction*)), SLOT(onWindowMenuItemClick(QAction*)));

    this->setStatusBar(0);

    mwDecorator_->customizeMainUi();
}

MainWindow::~MainWindow()
{
    delete ui_;
    mwDecorator_.reset();
}

void MainWindow::processGraph(const QString &filename)
{
    if (sceneRegister_.contains(filename))
    {
        QMessageBox::information(this, tr("Selected file is already opened."), tr("Selected file is alredy opened."));
        return;
    }

    BaseSceneGraphWindow* sgWin = SceneGraph::create(this);
    sgWin->openFile(filename);

    mwDecorator_->addSceneWindow(sgWin);

    connect(sgWin, SIGNAL(toBeDestroyed(QString)), SLOT(onDestroySceneWindow(const QString&)));

    //add ui stuff
    QAction* newWindow = windowListGroup_->addAction(filename);

    ui_->menuWindow->addAction(newWindow);
    newWindow->setCheckable(true);
    newWindow->setChecked(true);

    QListWidgetItem* listItem = new QListWidgetItem(filename);
    listItem->setToolTip(filename);
    ui_->graphList->addItem(listItem);

    //register in dictionary
    sceneRegister_.insert(filename, sgWin);

    //Hack to make window menu visible after open graph file.
    this->activateWindow();
    sgWin->activateWindow();
}

void MainWindow::openFile(const QString &fileName)
{
    processGraph(fileName);
}

void MainWindow::exportTo(const QString& fileName)
{
    BaseSceneGraphWindow* activeWindow = qobject_cast<BaseSceneGraphWindow*>(QApplication::activeWindow());

    if (activeWindow)
    {
        BaseSceneGraphWindow* sgWin = qobject_cast<BaseSceneGraphWindow*>(activeWindow);
        sgWin->exportTo(fileName);
    }
    else if (!fileName.isEmpty())
    {
        QMessageBox::information(this, tr("Warning"), tr("No graph is selected."));
    }
}

void MainWindow::onHandleMessage(const QString &fileName)
{
    if(fileName.isEmpty())
        return;

    processGraph(fileName);
}

void MainWindow::onFileOpen()
{
    QStringList fileNames =
            QFileDialog::getOpenFileNames(
                this,
                tr("Open DOT graph"),
                QString(),
                tr("DOT Graphs (*.dot *.dotty)"));

    for (const QString& fileName: fileNames)
    {
        openFile(fileName);
    }
}

void MainWindow::onHelpAbout()
{
    QMessageBox::about(this, tr("Graphviz Viewer"), tr("Graphviz Viewer %1").arg(PRODUCT_VERSION));
}

void MainWindow::onExport()
{
    QFileInfo fileInfo(this->windowFilePath());
    QDir documentDir = fileInfo.dir();
    QString baseName = fileInfo.baseName();
    baseName.replace(QRegExp("\\.dot(ty)$"), ".pdf");

    QString fileName =
            QFileDialog::getSaveFileName(
                this,
                tr("Export to..."),
                documentDir.filePath(baseName),
                tr("PDF Files (*.pdf);;PNG Files (*.png);;SVG Files (*.svg)"));

    exportTo(fileName);
}

void MainWindow::onWindowMenuItemClick(QAction* act)
{
   activateSelectedWindow(act->text());
}

void MainWindow::onClearSelected()
{
    for(auto i : ui_->graphList->selectedItems())
    {
        QString filename = i->text();
        auto window = sceneRegister_[filename];
        //Window could be already closed,
        //so just remove record from the graph list.
        if (!window.isNull())
            window->close();

        sceneRegister_.remove(filename);
     }
}

void MainWindow::activateSelectedWindow(const QString& label)
{
    if (sceneRegister_.contains(label))
    {
        sceneRegister_[label]->activateWindow();
        sceneRegister_[label]->raise();
    }

    mwDecorator_->activateSceneWindow(label);
}

void MainWindow::onItemDblCliked(QListWidgetItem* item)
{
    QString label = item->text();
    activateSelectedWindow(label);

    for(auto i : windowListGroup_->actions())
    {
        if (i->text() == label)
        {
            i->setChecked(true);
            break;
        }
    }
}

void MainWindow::onCompareItems()
{
    if (ui_->graphList->selectedItems().isEmpty())
    {
        QMessageBox::information(this, tr("Warning"), tr("No file is selected for compare."));
        return;
    }

    QVector<QString> files;

    for(auto i : ui_->graphList->selectedItems())
    {
        files.push_back(i->text());
    }

    if (diffWindow_.isNull())
        diffWindow_ = mwDecorator_->createCW();

    diffWindow_->showMaximized();

    diffWindow_->process(files);

    ui_->actionDiffWindow->setVisible(true);
}

void MainWindow::onRaiseCompareWindow()
{
    if (diffWindow_)
    {
        diffWindow_->activateWindow();
        diffWindow_->raise();
    }
}

void MainWindow::onDestroyCompareWindow()
{
    ui_->actionDiffWindow->setVisible(false);
}

void MainWindow::onDestroySceneWindow(const QString& filename)
{
    sceneRegister_.remove(filename);

    for(int i = 0; i < ui_->graphList->count(); ++i)
    {
        if (ui_->graphList->item(i)->text() == filename)
        {
            delete ui_->graphList->takeItem(i);
            break;
        }
    }

    QAction *actiontoRemove = 0;
    for(auto i : windowListGroup_->actions())
    {
        if (i->text() == filename)
        {
            actiontoRemove = i;
            break;
        }
    }

    windowListGroup_->removeAction(actiontoRemove);
    ui_->menuWindow->removeAction(actiontoRemove);

    mwDecorator_->removeSceneWindow(filename);
}

Ui::MainWindow* MainWindow::Ui()
{
    return ui_;
}
